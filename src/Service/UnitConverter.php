<?php
// src/Service/UnitConverter.php
namespace App\Service;

class UnitConverter
{
    public function convert($bytes, $precision=2)
    {
      $BIT_TO_MEGABIT = 1000000000;
      $KILOBYTES_TO_BITS = 8000;
      return round(($bytes * $KILOBYTES_TO_BITS) / $BIT_TO_MEGABIT, $precision);
    }
}