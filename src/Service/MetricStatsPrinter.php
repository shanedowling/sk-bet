<?php

// src/Service/MetricStatsPrinter.php
namespace App\Service;

class MetricStatsPrinter
{
  private $unitConverter;
  private $output;

  public function setUnitConverter(UnitConverter $unitConverter) {
    $this->unitConverter = $unitConverter;
  }

  public function setOutput(\Symfony\Component\Console\Output\OutputInterface $output) {
    $this->output = $output;
  }

  private function printInvestigations(Array $belowAveragePeriods) {
    $this->output->writeln('');
    $this->output->writeln('Investigate:');
    foreach($belowAveragePeriods as $belowAverage) {
      $this->output->writeln('');
      $this->output->writeln('    * The period between ' . $belowAverage[0] . ' and ' . $belowAverage[1]);
      $this->output->writeln('      was under-performing.');
      $this->output->writeln('');
    }
  }

  public function print(MetricDataProvider $metricData) {
      $this->output->writeln('SamKnows Metric Analyser v1.0.0');
      $this->output->writeln('===============================');
      $this->output->writeln('');
      $this->output->writeln('Period checked:');
      $this->output->writeln('');
      $this->output->writeln("    From: " . $metricData->from);
      $this->output->writeln("    To:   " . $metricData->to);
      $this->output->writeln('');
      $this->output->writeln('Statistics:');
      $this->output->writeln('');
      $this->output->writeln("    Unit: Megabits per second");
      $this->output->writeln('');
      $this->output->writeln("    Average: " . $this->unitConverter->convert($metricData->average));
      $this->output->writeln("    Min: " . $this->unitConverter->convert($metricData->min));
      $this->output->writeln("    Max: " . $this->unitConverter->convert($metricData->max));
      $this->output->writeln("    Median: " . $this->unitConverter->convert($metricData->median));
      if(count($metricData->belowAveragePeriods) > 0) {
        $this->printInvestigations($metricData->belowAveragePeriods);
      }
  }
}