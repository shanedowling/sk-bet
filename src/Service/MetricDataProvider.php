<?php
// src/Service/MetricDataProvider.php
namespace App\Service;

class MetricDataProvider
{
  private static $belowAverageMultiplier = 2; // Total guess as to what defines slow
  private $dataSet;
  public $min;
  public $max;
  public $median;
  public $from;
  public $to;
  public $average;
  public $belowAveragePeriods = [];

  private function calculateMedian(Array $arr) {
    $count = count($arr); //total numbers in array
    $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
    if($count % 2) { // odd number, middle is the median
        $median = $arr[$middleval]->metricValue;
    } else { // even number, calculate avg of 2 medians
        $low = $arr[$middleval]->metricValue;
        $high = $arr[$middleval+1]->metricValue;
        $median = (($low+$high)/2);
    }
    return $median;
  }
  
  private function detectBelowAverage(Array $metricDataSet, Float $average) {
    // TODO - I am not happy with this
    $slowSeries = [];
    $inSlowSeries =  false;
    $potentialSlowEnd = $slowStart = NULL;
    foreach($metricDataSet as $metric) {
      if(($metric->metricValue * self::$belowAverageMultiplier) < $average) {
        if(!$inSlowSeries) {
          $inSlowSeries = true;
          $slowStart = $metric->dtime;
        }
        $potentialSlowEnd = $metric->dtime;
      } else {
        if($inSlowSeries) {
          $inSlowSeries = false;
          $slowSeries[] = [$slowStart, $potentialSlowEnd];
        }
      }
    }
    if($inSlowSeries) {
      $slowSeries[] = [$slowStart, $potentialSlowEnd];
    }
    return $slowSeries;
  }

  public function loadData(Array $metricDataSet) {
    $this->from = reset($metricDataSet)->dtime;
    $this->to = end($metricDataSet)->dtime;

    $sum = array_reduce($metricDataSet, function($carry, $item)
    {
        return $carry + $item->metricValue;
    });
    $this->average = $sum/count($metricDataSet);
    $this->belowAveragePeriods = $this->detectBelowAverage($metricDataSet, $this->average);

    usort($metricDataSet, function($metricA, $metricB) {
      return $metricA->metricValue > $metricB->metricValue;
    });

    $this->min = reset($metricDataSet)->metricValue;
    $this->max = end($metricDataSet)->metricValue;
    $this->median = $this->calculateMedian($metricDataSet);
  }

}