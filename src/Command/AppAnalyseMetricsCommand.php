<?php declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\MetricDataProvider;
use App\Service\MetricStatsPrinter;
use App\Service\UnitConverter;

/**
 * Class AppAnalyseMetricsCommand
 *
 * @package App\Command
 */
class AppAnalyseMetricsCommand extends Command
{
    private $metricDataProvider;
    private $metricStatsPrinter;
    private $unitConverter;
    /**
     * @var string
     */
    protected static $defaultName = 'app:analyse-metrics';

    public function __construct(
      MetricDataProvider $metricDataProvider, 
      MetricStatsPrinter $metricStatsPrinter,
      UnitConverter $unitConverter
      ) {
        $this->metricDataProvider = $metricDataProvider;
        $this->metricStatsPrinter = $metricStatsPrinter;
        $this->metricStatsPrinter->setUnitConverter($unitConverter);
        parent::__construct();
    }

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        $this->setDescription('Analyses the metrics to generate a report.');
        $this->addOption('input', null, InputOption::VALUE_REQUIRED, 'The location of the test input');
    }

    /**
     * Detect slow-downs in the data and output them to stdout.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $speedMetrics = json_decode(file_get_contents($input->getOption('input')));
        $this->metricStatsPrinter->setOutput($output);
        foreach($speedMetrics->data as $metrics) {
          $this->metricDataProvider->loadData($metrics->metricData);
          $this->metricStatsPrinter->print($this->metricDataProvider);
        }
        return;
    }
}